from django import forms
from .models import Entry
from django.forms import TextInput

class CreateEntry(forms.ModelForm):
    class Meta:
        model = Entry
        fields = ('name', 'date', 'time', 'place', 'category',)
    def __init__(self, *args, **kwargs):
        super(CreateEntry, self).__init__(*args, **kwargs)
        self.fields['date'].widget = TextInput(attrs={
            'type': 'date',
            'class': 'dateinput',})
        self.fields['time'].widget = TextInput(attrs={
            'type': 'time',})