from django.shortcuts import render, redirect
from .forms import CreateEntry
from .models import Entry

# Create your views here.

# def create(request):
#     # entries = Entry.objects.all()
#     if request.method == "POST":
#         form = CreateEntry(request.POST)
#         if form.is_valid():
#             post = form.save(commit=False)
#             post.save()
#             return redirect('create:see')
#     else:
#         form = CreateEntry()
#     map = {
#         'form': form,
#     }
#     return render(request, 'create.html', map)



def create(request):
    form = CreateEntry(request.POST or None)
    # entries = Entry.objects.all().values()
    if (request.method == 'POST' and form.is_valid()):
        sched = {
            'name': request.POST['name'],
            'date': request.POST['date'],
            'time': request.POST['time'],
            'place': request.POST['place'],
            'category': request.POST['category'],
            # 'data': entries,
            'form': CreateEntry(),
        }

        new_entry = Entry(name=sched['name'], date=sched['date'], time=sched['time'], place=sched['place'], category=sched['category'])
        new_entry.save()
        return render(request, 'create.html', sched)
    else:
        map = {
            'form': CreateEntry(),
        }
        return render(request, 'create.html', map)
