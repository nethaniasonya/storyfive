from django.db import models

# Create your models here.
class Entry(models.Model):
    CATEGORIES = [
        ('Quiz', 'Quiz'),
        ('Exam', 'Exam'),
        ('Meeting', 'Meeting'),
        ('Other', 'Other'),
    ]
    name = models.CharField(max_length=30)
    date = models.DateField()
    time = models.TimeField()
    place = models.CharField(max_length=20)
    category = models.CharField(max_length=7, choices=CATEGORIES)

    def create(self):
        self.save()

    def __str__(self):
        return self.name

