from django.shortcuts import render, redirect
from create.models import Entry
# Create your views here.

def see(request):
    entries = Entry.objects.all()
    map = {
        'data': entries,
    }
    return render(request, 'schedule.html', map)

def delete(request, id):
   ent = Entry.objects.get(id = id)
   ent.delete()
   return render(request, 'schedule.html')

def delete_all(request):
    Entry.objects.all().delete()
    return redirect('see:see')