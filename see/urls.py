from django.urls import path
from . import views

app_name = 'see'

urlpatterns = [
    path('', views.see, name='see'),
    path('delete/<id>', views.delete, name='delete'),
    path('delete-all', views.delete_all, name='delete_all')
]